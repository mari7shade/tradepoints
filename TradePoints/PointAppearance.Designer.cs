﻿namespace TradePoints
{
    partial class PointAppearance
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PointAppearance));
            tableBeckground = new TableLayoutPanel();
            tableLayoutPanel2 = new TableLayoutPanel();
            PointTelephoneLabel = new Label();
            PointWorkingHoursLabel = new Label();
            PointOwnershipLabel = new Label();
            PointAddressLabel = new Label();
            PointSpecializationLabel = new Label();
            tableLayoutPanel3 = new TableLayoutPanel();
            NamePointLabel = new Label();
            pictureFavoritePoint = new PictureBox();
            pictureInfo = new PictureBox();
            pictureBox1 = new PictureBox();
            tableBeckground.SuspendLayout();
            tableLayoutPanel2.SuspendLayout();
            tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureFavoritePoint).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureInfo).BeginInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            SuspendLayout();
            // 
            // tableBeckground
            // 
            tableBeckground.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tableBeckground.ColumnCount = 2;
            tableBeckground.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 40F));
            tableBeckground.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 60F));
            tableBeckground.Controls.Add(tableLayoutPanel2, 1, 0);
            tableBeckground.Controls.Add(pictureBox1, 0, 0);
            tableBeckground.Cursor = Cursors.Hand;
            tableBeckground.Dock = DockStyle.Fill;
            tableBeckground.Location = new Point(0, 0);
            tableBeckground.Name = "tableBeckground";
            tableBeckground.RowCount = 1;
            tableBeckground.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableBeckground.Size = new Size(450, 200);
            tableBeckground.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2.ColumnCount = 1;
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel2.Controls.Add(PointTelephoneLabel, 0, 5);
            tableLayoutPanel2.Controls.Add(PointWorkingHoursLabel, 0, 4);
            tableLayoutPanel2.Controls.Add(PointOwnershipLabel, 0, 3);
            tableLayoutPanel2.Controls.Add(PointAddressLabel, 0, 2);
            tableLayoutPanel2.Controls.Add(PointSpecializationLabel, 0, 1);
            tableLayoutPanel2.Controls.Add(tableLayoutPanel3, 0, 0);
            tableLayoutPanel2.Dock = DockStyle.Fill;
            tableLayoutPanel2.Location = new Point(180, 1);
            tableLayoutPanel2.Margin = new Padding(0);
            tableLayoutPanel2.Name = "tableLayoutPanel2";
            tableLayoutPanel2.RowCount = 6;
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
            tableLayoutPanel2.Size = new Size(269, 198);
            tableLayoutPanel2.TabIndex = 0;
            // 
            // PointTelephoneLabel
            // 
            PointTelephoneLabel.AutoSize = true;
            PointTelephoneLabel.Dock = DockStyle.Fill;
            PointTelephoneLabel.Font = new Font("Segoe UI", 9.75F, FontStyle.Regular, GraphicsUnit.Point);
            PointTelephoneLabel.ImageAlign = ContentAlignment.BottomLeft;
            PointTelephoneLabel.Location = new Point(3, 165);
            PointTelephoneLabel.Name = "PointTelephoneLabel";
            PointTelephoneLabel.Size = new Size(263, 33);
            PointTelephoneLabel.TabIndex = 5;
            PointTelephoneLabel.Text = "label1";
            PointTelephoneLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // PointWorkingHoursLabel
            // 
            PointWorkingHoursLabel.AutoSize = true;
            PointWorkingHoursLabel.Dock = DockStyle.Fill;
            PointWorkingHoursLabel.Font = new Font("Segoe UI", 9.75F, FontStyle.Regular, GraphicsUnit.Point);
            PointWorkingHoursLabel.ImageAlign = ContentAlignment.BottomLeft;
            PointWorkingHoursLabel.Location = new Point(3, 132);
            PointWorkingHoursLabel.Name = "PointWorkingHoursLabel";
            PointWorkingHoursLabel.Size = new Size(263, 33);
            PointWorkingHoursLabel.TabIndex = 4;
            PointWorkingHoursLabel.Text = "label1";
            PointWorkingHoursLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // PointOwnershipLabel
            // 
            PointOwnershipLabel.AutoSize = true;
            PointOwnershipLabel.Dock = DockStyle.Fill;
            PointOwnershipLabel.Font = new Font("Segoe UI", 9.75F, FontStyle.Regular, GraphicsUnit.Point);
            PointOwnershipLabel.ImageAlign = ContentAlignment.BottomLeft;
            PointOwnershipLabel.Location = new Point(3, 99);
            PointOwnershipLabel.Name = "PointOwnershipLabel";
            PointOwnershipLabel.Size = new Size(263, 33);
            PointOwnershipLabel.TabIndex = 3;
            PointOwnershipLabel.Text = "label1";
            PointOwnershipLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // PointAddressLabel
            // 
            PointAddressLabel.AutoSize = true;
            PointAddressLabel.Dock = DockStyle.Fill;
            PointAddressLabel.Font = new Font("Segoe UI", 9.75F, FontStyle.Regular, GraphicsUnit.Point);
            PointAddressLabel.ImageAlign = ContentAlignment.BottomLeft;
            PointAddressLabel.Location = new Point(3, 66);
            PointAddressLabel.Name = "PointAddressLabel";
            PointAddressLabel.Size = new Size(263, 33);
            PointAddressLabel.TabIndex = 2;
            PointAddressLabel.Text = "label1";
            PointAddressLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // PointSpecializationLabel
            // 
            PointSpecializationLabel.AutoSize = true;
            PointSpecializationLabel.Dock = DockStyle.Fill;
            PointSpecializationLabel.Font = new Font("Segoe UI", 9.75F, FontStyle.Regular, GraphicsUnit.Point);
            PointSpecializationLabel.ImageAlign = ContentAlignment.BottomLeft;
            PointSpecializationLabel.Location = new Point(3, 33);
            PointSpecializationLabel.Name = "PointSpecializationLabel";
            PointSpecializationLabel.Size = new Size(263, 33);
            PointSpecializationLabel.TabIndex = 1;
            PointSpecializationLabel.Text = "label1";
            PointSpecializationLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel3
            // 
            tableLayoutPanel3.ColumnCount = 3;
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 56F));
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 55F));
            tableLayoutPanel3.Controls.Add(NamePointLabel, 0, 0);
            tableLayoutPanel3.Controls.Add(pictureFavoritePoint, 1, 0);
            tableLayoutPanel3.Controls.Add(pictureInfo, 2, 0);
            tableLayoutPanel3.Dock = DockStyle.Fill;
            tableLayoutPanel3.Location = new Point(3, 3);
            tableLayoutPanel3.Name = "tableLayoutPanel3";
            tableLayoutPanel3.RowCount = 1;
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel3.Size = new Size(263, 27);
            tableLayoutPanel3.TabIndex = 6;
            // 
            // NamePointLabel
            // 
            NamePointLabel.AutoSize = true;
            NamePointLabel.Dock = DockStyle.Fill;
            NamePointLabel.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            NamePointLabel.Location = new Point(3, 0);
            NamePointLabel.Name = "NamePointLabel";
            NamePointLabel.Size = new Size(146, 27);
            NamePointLabel.TabIndex = 1;
            NamePointLabel.Text = "label1";
            NamePointLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // pictureFavoritePoint
            // 
            pictureFavoritePoint.Image = Properties.Resources._2267;
            pictureFavoritePoint.Location = new Point(155, 3);
            pictureFavoritePoint.Name = "pictureFavoritePoint";
            pictureFavoritePoint.Size = new Size(49, 21);
            pictureFavoritePoint.SizeMode = PictureBoxSizeMode.Zoom;
            pictureFavoritePoint.TabIndex = 2;
            pictureFavoritePoint.TabStop = false;
            pictureFavoritePoint.Click += pictureFavoritePoint_Click;
            // 
            // pictureInfo
            // 
            pictureInfo.Cursor = Cursors.Help;
            pictureInfo.Image = (Image)resources.GetObject("pictureInfo.Image");
            pictureInfo.Location = new Point(211, 3);
            pictureInfo.Name = "pictureInfo";
            pictureInfo.Size = new Size(49, 21);
            pictureInfo.SizeMode = PictureBoxSizeMode.Zoom;
            pictureInfo.TabIndex = 3;
            pictureInfo.TabStop = false;
            pictureInfo.Click += pictureInfo_Click;
            // 
            // pictureBox1
            // 
            pictureBox1.BackColor = Color.Silver;
            pictureBox1.Dock = DockStyle.Fill;
            pictureBox1.Image = Properties.Resources.no_image_png_2;
            pictureBox1.Location = new Point(4, 4);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(172, 192);
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.TabIndex = 1;
            pictureBox1.TabStop = false;
            // 
            // PointAppearance
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(tableBeckground);
            Name = "PointAppearance";
            Size = new Size(450, 200);
            tableBeckground.ResumeLayout(false);
            tableLayoutPanel2.ResumeLayout(false);
            tableLayoutPanel2.PerformLayout();
            tableLayoutPanel3.ResumeLayout(false);
            tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureFavoritePoint).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureInfo).EndInit();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private TableLayoutPanel tableBeckground;
        private TableLayoutPanel tableLayoutPanel2;
        private Label PointSpecializationLabel;
        private Label PointTelephoneLabel;
        private Label PointWorkingHoursLabel;
        private Label PointOwnershipLabel;
        private Label PointAddressLabel;
        private PictureBox pictureBox1;
        private TableLayoutPanel tableLayoutPanel3;
        private Label NamePointLabel;
        private PictureBox pictureFavoritePoint;
        private PictureBox pictureInfo;
    }
}
