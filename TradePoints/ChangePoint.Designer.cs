﻿namespace TradePoints
{
    partial class ChangePoint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tableLayoutPanelMain = new TableLayoutPanel();
            tableLayoutPanel6 = new TableLayoutPanel();
            textBoxWorkingHours = new TextBox();
            labelWorkingHours = new Label();
            tableLayoutPanel5 = new TableLayoutPanel();
            textBoxOwnership = new TextBox();
            labelWorkForm = new Label();
            tableLayoutPanel4 = new TableLayoutPanel();
            textBoxPhone = new TextBox();
            labelPhone = new Label();
            tableLayoutPanel3 = new TableLayoutPanel();
            textBoxSpecialization = new TextBox();
            labelSpecialization = new Label();
            tableLayoutPanel2 = new TableLayoutPanel();
            textBoxAdress = new TextBox();
            labelAdress = new Label();
            tableLayoutPanel1 = new TableLayoutPanel();
            labelName = new Label();
            textBoxName = new TextBox();
            buttonClose = new Button();
            tableLayoutPanel7 = new TableLayoutPanel();
            textBoxImageURL = new TextBox();
            labelImageURL = new Label();
            AddButton = new Button();
            tableLayoutPanelMain.SuspendLayout();
            tableLayoutPanel6.SuspendLayout();
            tableLayoutPanel5.SuspendLayout();
            tableLayoutPanel4.SuspendLayout();
            tableLayoutPanel3.SuspendLayout();
            tableLayoutPanel2.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            tableLayoutPanel7.SuspendLayout();
            SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            tableLayoutPanelMain.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tableLayoutPanelMain.ColumnCount = 1;
            tableLayoutPanelMain.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanelMain.Controls.Add(tableLayoutPanel6, 0, 5);
            tableLayoutPanelMain.Controls.Add(tableLayoutPanel5, 0, 4);
            tableLayoutPanelMain.Controls.Add(tableLayoutPanel4, 0, 3);
            tableLayoutPanelMain.Controls.Add(tableLayoutPanel3, 0, 2);
            tableLayoutPanelMain.Controls.Add(tableLayoutPanel2, 0, 1);
            tableLayoutPanelMain.Controls.Add(tableLayoutPanel1, 0, 0);
            tableLayoutPanelMain.Controls.Add(buttonClose, 0, 8);
            tableLayoutPanelMain.Controls.Add(tableLayoutPanel7, 0, 6);
            tableLayoutPanelMain.Controls.Add(AddButton, 0, 7);
            tableLayoutPanelMain.Dock = DockStyle.Fill;
            tableLayoutPanelMain.Location = new Point(0, 0);
            tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            tableLayoutPanelMain.RowCount = 9;
            tableLayoutPanelMain.RowStyles.Add(new RowStyle(SizeType.Percent, 12F));
            tableLayoutPanelMain.RowStyles.Add(new RowStyle(SizeType.Percent, 12F));
            tableLayoutPanelMain.RowStyles.Add(new RowStyle(SizeType.Percent, 12F));
            tableLayoutPanelMain.RowStyles.Add(new RowStyle(SizeType.Percent, 12F));
            tableLayoutPanelMain.RowStyles.Add(new RowStyle(SizeType.Percent, 12F));
            tableLayoutPanelMain.RowStyles.Add(new RowStyle(SizeType.Percent, 12F));
            tableLayoutPanelMain.RowStyles.Add(new RowStyle(SizeType.Percent, 12F));
            tableLayoutPanelMain.RowStyles.Add(new RowStyle(SizeType.Percent, 8F));
            tableLayoutPanelMain.RowStyles.Add(new RowStyle(SizeType.Percent, 8F));
            tableLayoutPanelMain.Size = new Size(594, 736);
            tableLayoutPanelMain.TabIndex = 2;
            // 
            // tableLayoutPanel6
            // 
            tableLayoutPanel6.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            tableLayoutPanel6.ColumnCount = 1;
            tableLayoutPanel6.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel6.Controls.Add(textBoxWorkingHours, 0, 1);
            tableLayoutPanel6.Controls.Add(labelWorkingHours, 0, 0);
            tableLayoutPanel6.Location = new Point(1, 441);
            tableLayoutPanel6.Margin = new Padding(0);
            tableLayoutPanel6.Name = "tableLayoutPanel6";
            tableLayoutPanel6.RowCount = 2;
            tableLayoutPanel6.RowStyles.Add(new RowStyle(SizeType.Percent, 40.9090919F));
            tableLayoutPanel6.RowStyles.Add(new RowStyle(SizeType.Percent, 59.0909081F));
            tableLayoutPanel6.Size = new Size(592, 84);
            tableLayoutPanel6.TabIndex = 5;
            // 
            // textBoxWorkingHours
            // 
            textBoxWorkingHours.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            textBoxWorkingHours.BackColor = Color.GhostWhite;
            textBoxWorkingHours.BorderStyle = BorderStyle.None;
            textBoxWorkingHours.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBoxWorkingHours.Location = new Point(3, 37);
            textBoxWorkingHours.MinimumSize = new Size(0, 40);
            textBoxWorkingHours.Name = "textBoxWorkingHours";
            textBoxWorkingHours.Size = new Size(586, 40);
            textBoxWorkingHours.TabIndex = 3;
            textBoxWorkingHours.TextAlign = HorizontalAlignment.Center;
            // 
            // labelWorkingHours
            // 
            labelWorkingHours.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            labelWorkingHours.AutoSize = true;
            labelWorkingHours.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            labelWorkingHours.Location = new Point(3, 0);
            labelWorkingHours.Name = "labelWorkingHours";
            labelWorkingHours.Size = new Size(586, 34);
            labelWorkingHours.TabIndex = 1;
            labelWorkingHours.Text = "Час роботи:";
            labelWorkingHours.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel5
            // 
            tableLayoutPanel5.ColumnCount = 1;
            tableLayoutPanel5.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel5.Controls.Add(textBoxOwnership, 0, 1);
            tableLayoutPanel5.Controls.Add(labelWorkForm, 0, 0);
            tableLayoutPanel5.Dock = DockStyle.Fill;
            tableLayoutPanel5.Location = new Point(1, 353);
            tableLayoutPanel5.Margin = new Padding(0);
            tableLayoutPanel5.Name = "tableLayoutPanel5";
            tableLayoutPanel5.RowCount = 2;
            tableLayoutPanel5.RowStyles.Add(new RowStyle(SizeType.Percent, 40.9090919F));
            tableLayoutPanel5.RowStyles.Add(new RowStyle(SizeType.Percent, 59.0909081F));
            tableLayoutPanel5.Size = new Size(592, 87);
            tableLayoutPanel5.TabIndex = 4;
            // 
            // textBoxOwnership
            // 
            textBoxOwnership.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            textBoxOwnership.BackColor = Color.GhostWhite;
            textBoxOwnership.BorderStyle = BorderStyle.None;
            textBoxOwnership.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBoxOwnership.Location = new Point(3, 38);
            textBoxOwnership.MinimumSize = new Size(0, 40);
            textBoxOwnership.Name = "textBoxOwnership";
            textBoxOwnership.Size = new Size(586, 40);
            textBoxOwnership.TabIndex = 2;
            textBoxOwnership.TextAlign = HorizontalAlignment.Center;
            // 
            // labelWorkForm
            // 
            labelWorkForm.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            labelWorkForm.AutoSize = true;
            labelWorkForm.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            labelWorkForm.Location = new Point(3, 0);
            labelWorkForm.Name = "labelWorkForm";
            labelWorkForm.Size = new Size(586, 35);
            labelWorkForm.TabIndex = 1;
            labelWorkForm.Text = "Форма власності:";
            labelWorkForm.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel4
            // 
            tableLayoutPanel4.ColumnCount = 1;
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel4.Controls.Add(textBoxPhone, 0, 1);
            tableLayoutPanel4.Controls.Add(labelPhone, 0, 0);
            tableLayoutPanel4.Dock = DockStyle.Fill;
            tableLayoutPanel4.Location = new Point(1, 265);
            tableLayoutPanel4.Margin = new Padding(0);
            tableLayoutPanel4.Name = "tableLayoutPanel4";
            tableLayoutPanel4.RowCount = 2;
            tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Percent, 40.9090919F));
            tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Percent, 59.0909081F));
            tableLayoutPanel4.Size = new Size(592, 87);
            tableLayoutPanel4.TabIndex = 3;
            // 
            // textBoxPhone
            // 
            textBoxPhone.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            textBoxPhone.BackColor = Color.GhostWhite;
            textBoxPhone.BorderStyle = BorderStyle.None;
            textBoxPhone.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBoxPhone.Location = new Point(3, 38);
            textBoxPhone.MinimumSize = new Size(0, 40);
            textBoxPhone.Name = "textBoxPhone";
            textBoxPhone.Size = new Size(586, 40);
            textBoxPhone.TabIndex = 2;
            textBoxPhone.TextAlign = HorizontalAlignment.Center;
            // 
            // labelPhone
            // 
            labelPhone.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            labelPhone.AutoSize = true;
            labelPhone.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            labelPhone.Location = new Point(3, 0);
            labelPhone.Name = "labelPhone";
            labelPhone.Size = new Size(586, 35);
            labelPhone.TabIndex = 1;
            labelPhone.Text = "Телефони:";
            labelPhone.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            tableLayoutPanel3.ColumnCount = 1;
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel3.Controls.Add(textBoxSpecialization, 0, 1);
            tableLayoutPanel3.Controls.Add(labelSpecialization, 0, 0);
            tableLayoutPanel3.Dock = DockStyle.Fill;
            tableLayoutPanel3.Location = new Point(1, 177);
            tableLayoutPanel3.Margin = new Padding(0);
            tableLayoutPanel3.Name = "tableLayoutPanel3";
            tableLayoutPanel3.RowCount = 2;
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Percent, 40.9090919F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Percent, 59.0909081F));
            tableLayoutPanel3.Size = new Size(592, 87);
            tableLayoutPanel3.TabIndex = 2;
            // 
            // textBoxSpecialization
            // 
            textBoxSpecialization.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            textBoxSpecialization.BackColor = Color.GhostWhite;
            textBoxSpecialization.BorderStyle = BorderStyle.None;
            textBoxSpecialization.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBoxSpecialization.Location = new Point(3, 38);
            textBoxSpecialization.MinimumSize = new Size(0, 40);
            textBoxSpecialization.Name = "textBoxSpecialization";
            textBoxSpecialization.Size = new Size(586, 40);
            textBoxSpecialization.TabIndex = 2;
            textBoxSpecialization.TextAlign = HorizontalAlignment.Center;
            // 
            // labelSpecialization
            // 
            labelSpecialization.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            labelSpecialization.AutoSize = true;
            labelSpecialization.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            labelSpecialization.Location = new Point(3, 0);
            labelSpecialization.Name = "labelSpecialization";
            labelSpecialization.Size = new Size(586, 35);
            labelSpecialization.TabIndex = 1;
            labelSpecialization.Text = "Спеціалізація:";
            labelSpecialization.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2.ColumnCount = 1;
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel2.Controls.Add(textBoxAdress, 0, 1);
            tableLayoutPanel2.Controls.Add(labelAdress, 0, 0);
            tableLayoutPanel2.Dock = DockStyle.Fill;
            tableLayoutPanel2.Location = new Point(1, 89);
            tableLayoutPanel2.Margin = new Padding(0);
            tableLayoutPanel2.Name = "tableLayoutPanel2";
            tableLayoutPanel2.RowCount = 2;
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 40.9090919F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 59.0909081F));
            tableLayoutPanel2.Size = new Size(592, 87);
            tableLayoutPanel2.TabIndex = 1;
            // 
            // textBoxAdress
            // 
            textBoxAdress.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            textBoxAdress.BackColor = Color.GhostWhite;
            textBoxAdress.BorderStyle = BorderStyle.None;
            textBoxAdress.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBoxAdress.Location = new Point(3, 38);
            textBoxAdress.MinimumSize = new Size(0, 40);
            textBoxAdress.Name = "textBoxAdress";
            textBoxAdress.Size = new Size(586, 40);
            textBoxAdress.TabIndex = 2;
            textBoxAdress.TextAlign = HorizontalAlignment.Center;
            // 
            // labelAdress
            // 
            labelAdress.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            labelAdress.AutoSize = true;
            labelAdress.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            labelAdress.Location = new Point(3, 0);
            labelAdress.Name = "labelAdress";
            labelAdress.Size = new Size(586, 35);
            labelAdress.TabIndex = 1;
            labelAdress.Text = "Адреса:";
            labelAdress.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Controls.Add(labelName, 0, 0);
            tableLayoutPanel1.Controls.Add(textBoxName, 0, 1);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(1, 1);
            tableLayoutPanel1.Margin = new Padding(0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 2;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 35F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 65F));
            tableLayoutPanel1.Size = new Size(592, 87);
            tableLayoutPanel1.TabIndex = 0;
            // 
            // labelName
            // 
            labelName.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            labelName.AutoSize = true;
            labelName.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            labelName.Location = new Point(3, 0);
            labelName.Name = "labelName";
            labelName.Size = new Size(586, 30);
            labelName.TabIndex = 0;
            labelName.Text = "Назва:";
            labelName.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // textBoxName
            // 
            textBoxName.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            textBoxName.BackColor = Color.GhostWhite;
            textBoxName.BorderStyle = BorderStyle.None;
            textBoxName.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBoxName.Location = new Point(3, 33);
            textBoxName.MinimumSize = new Size(0, 40);
            textBoxName.Name = "textBoxName";
            textBoxName.Size = new Size(586, 40);
            textBoxName.TabIndex = 1;
            textBoxName.TextAlign = HorizontalAlignment.Center;
            // 
            // buttonClose
            // 
            buttonClose.Anchor = AnchorStyles.Top;
            buttonClose.BackColor = Color.Silver;
            buttonClose.FlatStyle = FlatStyle.Flat;
            buttonClose.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            buttonClose.Location = new Point(247, 679);
            buttonClose.Name = "buttonClose";
            buttonClose.Size = new Size(99, 34);
            buttonClose.TabIndex = 7;
            buttonClose.Text = "Відмінити";
            buttonClose.UseVisualStyleBackColor = false;
            buttonClose.Click += buttonClose_Click;
            // 
            // tableLayoutPanel7
            // 
            tableLayoutPanel7.ColumnCount = 1;
            tableLayoutPanel7.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel7.Controls.Add(textBoxImageURL, 0, 1);
            tableLayoutPanel7.Controls.Add(labelImageURL, 0, 0);
            tableLayoutPanel7.Dock = DockStyle.Fill;
            tableLayoutPanel7.Location = new Point(4, 532);
            tableLayoutPanel7.Name = "tableLayoutPanel7";
            tableLayoutPanel7.RowCount = 2;
            tableLayoutPanel7.RowStyles.Add(new RowStyle(SizeType.Percent, 35F));
            tableLayoutPanel7.RowStyles.Add(new RowStyle(SizeType.Percent, 65F));
            tableLayoutPanel7.Size = new Size(586, 81);
            tableLayoutPanel7.TabIndex = 8;
            // 
            // textBoxImageURL
            // 
            textBoxImageURL.BackColor = Color.GhostWhite;
            textBoxImageURL.BorderStyle = BorderStyle.None;
            textBoxImageURL.Dock = DockStyle.Fill;
            textBoxImageURL.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            textBoxImageURL.Location = new Point(3, 31);
            textBoxImageURL.MinimumSize = new Size(0, 40);
            textBoxImageURL.Name = "textBoxImageURL";
            textBoxImageURL.Size = new Size(580, 40);
            textBoxImageURL.TabIndex = 5;
            textBoxImageURL.TextAlign = HorizontalAlignment.Center;
            // 
            // labelImageURL
            // 
            labelImageURL.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            labelImageURL.AutoSize = true;
            labelImageURL.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            labelImageURL.Location = new Point(3, 0);
            labelImageURL.Name = "labelImageURL";
            labelImageURL.Size = new Size(580, 28);
            labelImageURL.TabIndex = 3;
            labelImageURL.Text = "Посилання на зображення:";
            labelImageURL.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // AddButton
            // 
            AddButton.Anchor = AnchorStyles.Top;
            AddButton.BackColor = Color.Silver;
            AddButton.FlatStyle = FlatStyle.Flat;
            AddButton.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            AddButton.Location = new Point(245, 620);
            AddButton.Name = "AddButton";
            AddButton.Size = new Size(103, 43);
            AddButton.TabIndex = 6;
            AddButton.Text = "Редагувати";
            AddButton.UseVisualStyleBackColor = false;
            AddButton.Click += AddButton_Click;
            // 
            // ChangePoint
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(594, 736);
            Controls.Add(tableLayoutPanelMain);
            Name = "ChangePoint";
            Text = "ChangePoint";
            Load += ChangePoint_Load;
            tableLayoutPanelMain.ResumeLayout(false);
            tableLayoutPanel6.ResumeLayout(false);
            tableLayoutPanel6.PerformLayout();
            tableLayoutPanel5.ResumeLayout(false);
            tableLayoutPanel5.PerformLayout();
            tableLayoutPanel4.ResumeLayout(false);
            tableLayoutPanel4.PerformLayout();
            tableLayoutPanel3.ResumeLayout(false);
            tableLayoutPanel3.PerformLayout();
            tableLayoutPanel2.ResumeLayout(false);
            tableLayoutPanel2.PerformLayout();
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel1.PerformLayout();
            tableLayoutPanel7.ResumeLayout(false);
            tableLayoutPanel7.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private TableLayoutPanel tableLayoutPanelMain;
        private Button buttonClose;
        private TableLayoutPanel tableLayoutPanel6;
        private TextBox textBoxWorkingHours;
        private Label labelWorkingHours;
        private TableLayoutPanel tableLayoutPanel5;
        private TextBox textBoxOwnership;
        private Label labelWorkForm;
        private TableLayoutPanel tableLayoutPanel4;
        private TextBox textBoxPhone;
        private Label labelPhone;
        private TableLayoutPanel tableLayoutPanel3;
        private TextBox textBoxSpecialization;
        private Label labelSpecialization;
        private TableLayoutPanel tableLayoutPanel2;
        private TextBox textBoxAdress;
        private Label labelAdress;
        private TableLayoutPanel tableLayoutPanel1;
        private Label labelName;
        private TextBox textBoxName;
        private Button AddButton;
        private TableLayoutPanel tableLayoutPanel7;
        private Label labelImageURL;
        private TextBox textBoxImageURL;
    }
}