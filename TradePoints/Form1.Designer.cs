﻿namespace TradePoints
{
    partial class Shops
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Shops));
            TaibleMainMenu = new TableLayoutPanel();
            ExitButton = new Button();
            TradePointButton = new Button();
            FavoriteButton = new Button();
            TableAllPoints = new TableLayoutPanel();
            PanelAllPoints = new FlowLayoutPanel();
            TableAllSearch = new TableLayoutPanel();
            tableLayoutPanel1 = new TableLayoutPanel();
            searchByAdress = new TextBox();
            labelAdressSearch = new Label();
            AllPointsBackAndName = new TableLayoutPanel();
            AllPointsBackButton = new Button();
            AllPointsName = new Label();
            SearchNameTable = new TableLayoutPanel();
            labelNameSearch = new Label();
            searchByName = new TextBox();
            tableLayoutPanel2 = new TableLayoutPanel();
            searchBySpecialization = new TextBox();
            labelSpecializationSearch = new Label();
            pictureSearch = new PictureBox();
            tableForActionByPoints = new TableLayoutPanel();
            buttonDeletePoint = new Button();
            ChangePoint = new Button();
            buttonAddPoint = new Button();
            buttonAddFavorite = new Button();
            TableFavoritePoints = new TableLayoutPanel();
            PanelFavoritePoints = new FlowLayoutPanel();
            tableFavoriteTop = new TableLayoutPanel();
            tableLayoutPanel6 = new TableLayoutPanel();
            buttonFavoriteBack = new Button();
            labelFavoritePoints = new Label();
            tableFavoriteButtonTop = new TableLayoutPanel();
            SaveFavorite = new Button();
            DeleteFavorite = new Button();
            saveFileDialog1 = new SaveFileDialog();
            TaibleMainMenu.SuspendLayout();
            TableAllPoints.SuspendLayout();
            TableAllSearch.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            AllPointsBackAndName.SuspendLayout();
            SearchNameTable.SuspendLayout();
            tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureSearch).BeginInit();
            tableForActionByPoints.SuspendLayout();
            TableFavoritePoints.SuspendLayout();
            tableFavoriteTop.SuspendLayout();
            tableLayoutPanel6.SuspendLayout();
            tableFavoriteButtonTop.SuspendLayout();
            SuspendLayout();
            // 
            // TaibleMainMenu
            // 
            TaibleMainMenu.ColumnCount = 1;
            TaibleMainMenu.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            TaibleMainMenu.Controls.Add(ExitButton, 0, 2);
            TaibleMainMenu.Controls.Add(TradePointButton, 0, 0);
            TaibleMainMenu.Controls.Add(FavoriteButton, 0, 1);
            TaibleMainMenu.Dock = DockStyle.Fill;
            TaibleMainMenu.Location = new Point(0, 0);
            TaibleMainMenu.Name = "TaibleMainMenu";
            TaibleMainMenu.RowCount = 3;
            TaibleMainMenu.RowStyles.Add(new RowStyle(SizeType.Percent, 33.3333321F));
            TaibleMainMenu.RowStyles.Add(new RowStyle(SizeType.Percent, 33.3333321F));
            TaibleMainMenu.RowStyles.Add(new RowStyle(SizeType.Percent, 33.3333321F));
            TaibleMainMenu.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            TaibleMainMenu.Size = new Size(945, 724);
            TaibleMainMenu.TabIndex = 0;
            // 
            // ExitButton
            // 
            ExitButton.Anchor = AnchorStyles.None;
            ExitButton.BackColor = Color.LightGray;
            ExitButton.Cursor = Cursors.Hand;
            ExitButton.FlatAppearance.BorderColor = Color.Gray;
            ExitButton.FlatAppearance.BorderSize = 0;
            ExitButton.FlatAppearance.MouseDownBackColor = Color.Silver;
            ExitButton.FlatAppearance.MouseOverBackColor = Color.Silver;
            ExitButton.FlatStyle = FlatStyle.Flat;
            ExitButton.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            ExitButton.ForeColor = Color.Black;
            ExitButton.Location = new Point(347, 563);
            ExitButton.Name = "ExitButton";
            ExitButton.Size = new Size(251, 79);
            ExitButton.TabIndex = 2;
            ExitButton.Text = "Вийти";
            ExitButton.UseVisualStyleBackColor = false;
            ExitButton.Click += ExitButton_Click;
            // 
            // TradePointButton
            // 
            TradePointButton.Anchor = AnchorStyles.None;
            TradePointButton.BackColor = Color.LightGray;
            TradePointButton.Cursor = Cursors.Hand;
            TradePointButton.FlatAppearance.BorderColor = Color.Gray;
            TradePointButton.FlatAppearance.BorderSize = 0;
            TradePointButton.FlatAppearance.MouseDownBackColor = Color.Silver;
            TradePointButton.FlatAppearance.MouseOverBackColor = Color.Silver;
            TradePointButton.FlatStyle = FlatStyle.Flat;
            TradePointButton.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            TradePointButton.ForeColor = Color.Black;
            TradePointButton.Location = new Point(347, 81);
            TradePointButton.Name = "TradePointButton";
            TradePointButton.Size = new Size(251, 79);
            TradePointButton.TabIndex = 0;
            TradePointButton.Text = "Всі торгові точки";
            TradePointButton.UseVisualStyleBackColor = false;
            TradePointButton.Click += TradePointButton_Click;
            // 
            // FavoriteButton
            // 
            FavoriteButton.Anchor = AnchorStyles.None;
            FavoriteButton.BackColor = Color.LightGray;
            FavoriteButton.Cursor = Cursors.Hand;
            FavoriteButton.FlatAppearance.BorderColor = Color.Gray;
            FavoriteButton.FlatAppearance.BorderSize = 0;
            FavoriteButton.FlatAppearance.MouseDownBackColor = Color.Silver;
            FavoriteButton.FlatAppearance.MouseOverBackColor = Color.Silver;
            FavoriteButton.FlatStyle = FlatStyle.Flat;
            FavoriteButton.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            FavoriteButton.ForeColor = Color.Black;
            FavoriteButton.Location = new Point(347, 322);
            FavoriteButton.Name = "FavoriteButton";
            FavoriteButton.Size = new Size(251, 79);
            FavoriteButton.TabIndex = 1;
            FavoriteButton.Text = "Вибране";
            FavoriteButton.UseVisualStyleBackColor = false;
            FavoriteButton.Click += FavoriteButton_Click;
            // 
            // TableAllPoints
            // 
            TableAllPoints.ColumnCount = 1;
            TableAllPoints.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            TableAllPoints.Controls.Add(PanelAllPoints, 0, 1);
            TableAllPoints.Controls.Add(TableAllSearch, 0, 0);
            TableAllPoints.Dock = DockStyle.Fill;
            TableAllPoints.Location = new Point(0, 0);
            TableAllPoints.Name = "TableAllPoints";
            TableAllPoints.RowCount = 2;
            TableAllPoints.RowStyles.Add(new RowStyle(SizeType.Percent, 30.5970154F));
            TableAllPoints.RowStyles.Add(new RowStyle(SizeType.Percent, 69.4029846F));
            TableAllPoints.Size = new Size(945, 724);
            TableAllPoints.TabIndex = 1;
            // 
            // PanelAllPoints
            // 
            PanelAllPoints.AutoScroll = true;
            PanelAllPoints.Dock = DockStyle.Fill;
            PanelAllPoints.Location = new Point(0, 221);
            PanelAllPoints.Margin = new Padding(0);
            PanelAllPoints.Name = "PanelAllPoints";
            PanelAllPoints.Size = new Size(945, 503);
            PanelAllPoints.TabIndex = 1;
            // 
            // TableAllSearch
            // 
            TableAllSearch.ColumnCount = 3;
            TableAllSearch.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            TableAllSearch.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            TableAllSearch.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            TableAllSearch.Controls.Add(tableLayoutPanel1, 0, 3);
            TableAllSearch.Controls.Add(AllPointsBackAndName, 0, 0);
            TableAllSearch.Controls.Add(SearchNameTable, 0, 1);
            TableAllSearch.Controls.Add(tableLayoutPanel2, 0, 2);
            TableAllSearch.Controls.Add(pictureSearch, 1, 1);
            TableAllSearch.Controls.Add(tableForActionByPoints, 2, 1);
            TableAllSearch.Dock = DockStyle.Fill;
            TableAllSearch.Location = new Point(3, 3);
            TableAllSearch.Name = "TableAllSearch";
            TableAllSearch.RowCount = 4;
            TableAllSearch.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
            TableAllSearch.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
            TableAllSearch.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
            TableAllSearch.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
            TableAllSearch.Size = new Size(939, 215);
            TableAllSearch.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Controls.Add(searchByAdress, 0, 1);
            tableLayoutPanel1.Controls.Add(labelAdressSearch, 0, 0);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 159);
            tableLayoutPanel1.Margin = new Padding(0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 2;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.Size = new Size(234, 56);
            tableLayoutPanel1.TabIndex = 3;
            // 
            // searchByAdress
            // 
            searchByAdress.BackColor = Color.LightGray;
            searchByAdress.BorderStyle = BorderStyle.FixedSingle;
            searchByAdress.Cursor = Cursors.IBeam;
            searchByAdress.Dock = DockStyle.Fill;
            searchByAdress.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            searchByAdress.Location = new Point(0, 28);
            searchByAdress.Margin = new Padding(0);
            searchByAdress.MaxLength = 100;
            searchByAdress.Name = "searchByAdress";
            searchByAdress.Size = new Size(234, 27);
            searchByAdress.TabIndex = 2;
            searchByAdress.TextChanged += searchByAdress_TextChanged;
            // 
            // labelAdressSearch
            // 
            labelAdressSearch.AutoSize = true;
            labelAdressSearch.Dock = DockStyle.Fill;
            labelAdressSearch.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            labelAdressSearch.Location = new Point(0, 0);
            labelAdressSearch.Margin = new Padding(0);
            labelAdressSearch.Name = "labelAdressSearch";
            labelAdressSearch.Size = new Size(234, 28);
            labelAdressSearch.TabIndex = 0;
            labelAdressSearch.Text = "За адресою:";
            labelAdressSearch.TextAlign = ContentAlignment.BottomLeft;
            // 
            // AllPointsBackAndName
            // 
            AllPointsBackAndName.ColumnCount = 2;
            AllPointsBackAndName.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 30F));
            AllPointsBackAndName.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            AllPointsBackAndName.Controls.Add(AllPointsBackButton, 0, 0);
            AllPointsBackAndName.Controls.Add(AllPointsName, 1, 0);
            AllPointsBackAndName.Dock = DockStyle.Fill;
            AllPointsBackAndName.Location = new Point(3, 3);
            AllPointsBackAndName.Name = "AllPointsBackAndName";
            AllPointsBackAndName.RowCount = 1;
            AllPointsBackAndName.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            AllPointsBackAndName.Size = new Size(228, 47);
            AllPointsBackAndName.TabIndex = 1;
            // 
            // AllPointsBackButton
            // 
            AllPointsBackButton.BackColor = Color.Transparent;
            AllPointsBackButton.Cursor = Cursors.Hand;
            AllPointsBackButton.Dock = DockStyle.Fill;
            AllPointsBackButton.FlatAppearance.BorderSize = 0;
            AllPointsBackButton.FlatStyle = FlatStyle.Flat;
            AllPointsBackButton.ForeColor = Color.Transparent;
            AllPointsBackButton.Image = (Image)resources.GetObject("AllPointsBackButton.Image");
            AllPointsBackButton.Location = new Point(3, 3);
            AllPointsBackButton.Name = "AllPointsBackButton";
            AllPointsBackButton.Size = new Size(24, 41);
            AllPointsBackButton.TabIndex = 0;
            AllPointsBackButton.UseVisualStyleBackColor = false;
            AllPointsBackButton.Click += AllPointsBackButton_Click;
            // 
            // AllPointsName
            // 
            AllPointsName.AutoSize = true;
            AllPointsName.BackColor = Color.Transparent;
            AllPointsName.Dock = DockStyle.Fill;
            AllPointsName.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            AllPointsName.Location = new Point(33, 0);
            AllPointsName.Name = "AllPointsName";
            AllPointsName.Size = new Size(192, 47);
            AllPointsName.TabIndex = 1;
            AllPointsName.Text = "Торгові Точки";
            AllPointsName.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // SearchNameTable
            // 
            SearchNameTable.ColumnCount = 1;
            SearchNameTable.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            SearchNameTable.Controls.Add(labelNameSearch, 0, 0);
            SearchNameTable.Controls.Add(searchByName, 0, 1);
            SearchNameTable.Dock = DockStyle.Fill;
            SearchNameTable.Location = new Point(0, 53);
            SearchNameTable.Margin = new Padding(0);
            SearchNameTable.Name = "SearchNameTable";
            SearchNameTable.RowCount = 2;
            SearchNameTable.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            SearchNameTable.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            SearchNameTable.Size = new Size(234, 53);
            SearchNameTable.TabIndex = 2;
            // 
            // labelNameSearch
            // 
            labelNameSearch.AutoSize = true;
            labelNameSearch.Dock = DockStyle.Fill;
            labelNameSearch.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            labelNameSearch.Location = new Point(0, 0);
            labelNameSearch.Margin = new Padding(0);
            labelNameSearch.Name = "labelNameSearch";
            labelNameSearch.Size = new Size(234, 26);
            labelNameSearch.TabIndex = 0;
            labelNameSearch.Text = "За назвою підприємства:";
            labelNameSearch.TextAlign = ContentAlignment.BottomLeft;
            // 
            // searchByName
            // 
            searchByName.BackColor = Color.LightGray;
            searchByName.BorderStyle = BorderStyle.FixedSingle;
            searchByName.Cursor = Cursors.IBeam;
            searchByName.Dock = DockStyle.Fill;
            searchByName.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            searchByName.Location = new Point(0, 26);
            searchByName.Margin = new Padding(0);
            searchByName.MaxLength = 100;
            searchByName.Name = "searchByName";
            searchByName.Size = new Size(234, 27);
            searchByName.TabIndex = 1;
            searchByName.TextChanged += searchByName_TextChanged;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2.ColumnCount = 1;
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel2.Controls.Add(searchBySpecialization, 0, 1);
            tableLayoutPanel2.Controls.Add(labelSpecializationSearch, 0, 0);
            tableLayoutPanel2.Dock = DockStyle.Fill;
            tableLayoutPanel2.Location = new Point(0, 106);
            tableLayoutPanel2.Margin = new Padding(0);
            tableLayoutPanel2.Name = "tableLayoutPanel2";
            tableLayoutPanel2.RowCount = 2;
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel2.Size = new Size(234, 53);
            tableLayoutPanel2.TabIndex = 4;
            // 
            // searchBySpecialization
            // 
            searchBySpecialization.BackColor = Color.LightGray;
            searchBySpecialization.BorderStyle = BorderStyle.FixedSingle;
            searchBySpecialization.Cursor = Cursors.IBeam;
            searchBySpecialization.Dock = DockStyle.Fill;
            searchBySpecialization.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            searchBySpecialization.Location = new Point(0, 26);
            searchBySpecialization.Margin = new Padding(0);
            searchBySpecialization.MaxLength = 100;
            searchBySpecialization.Name = "searchBySpecialization";
            searchBySpecialization.Size = new Size(234, 27);
            searchBySpecialization.TabIndex = 2;
            searchBySpecialization.TextChanged += searchBySpecialization_TextChanged;
            // 
            // labelSpecializationSearch
            // 
            labelSpecializationSearch.AutoSize = true;
            labelSpecializationSearch.Dock = DockStyle.Fill;
            labelSpecializationSearch.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            labelSpecializationSearch.Location = new Point(0, 0);
            labelSpecializationSearch.Margin = new Padding(0);
            labelSpecializationSearch.Name = "labelSpecializationSearch";
            labelSpecializationSearch.Size = new Size(234, 26);
            labelSpecializationSearch.TabIndex = 1;
            labelSpecializationSearch.Text = "За спеціалізацією:";
            labelSpecializationSearch.TextAlign = ContentAlignment.BottomLeft;
            // 
            // pictureSearch
            // 
            pictureSearch.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            pictureSearch.Image = Properties.Resources.search_icon_transparent_images_vector_15;
            pictureSearch.Location = new Point(237, 77);
            pictureSearch.Name = "pictureSearch";
            pictureSearch.Size = new Size(43, 26);
            pictureSearch.SizeMode = PictureBoxSizeMode.Zoom;
            pictureSearch.TabIndex = 5;
            pictureSearch.TabStop = false;
            // 
            // tableForActionByPoints
            // 
            tableForActionByPoints.ColumnCount = 4;
            tableForActionByPoints.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 16.6633339F));
            tableForActionByPoints.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 16.6633339F));
            tableForActionByPoints.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 16.6633339F));
            tableForActionByPoints.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.010006F));
            tableForActionByPoints.Controls.Add(buttonDeletePoint, 0, 0);
            tableForActionByPoints.Controls.Add(ChangePoint, 0, 0);
            tableForActionByPoints.Controls.Add(buttonAddPoint, 0, 0);
            tableForActionByPoints.Controls.Add(buttonAddFavorite, 0, 0);
            tableForActionByPoints.Dock = DockStyle.Fill;
            tableForActionByPoints.Location = new Point(471, 56);
            tableForActionByPoints.Name = "tableForActionByPoints";
            tableForActionByPoints.RowCount = 1;
            tableForActionByPoints.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableForActionByPoints.Size = new Size(465, 47);
            tableForActionByPoints.TabIndex = 6;
            // 
            // buttonDeletePoint
            // 
            buttonDeletePoint.BackColor = Color.Transparent;
            buttonDeletePoint.BackgroundImage = Properties.Resources.image_processing20210629_14744_1rjf38b;
            buttonDeletePoint.BackgroundImageLayout = ImageLayout.Center;
            buttonDeletePoint.Cursor = Cursors.Hand;
            buttonDeletePoint.Dock = DockStyle.Fill;
            buttonDeletePoint.FlatAppearance.BorderSize = 0;
            buttonDeletePoint.FlatStyle = FlatStyle.Flat;
            buttonDeletePoint.Location = new Point(157, 3);
            buttonDeletePoint.Name = "buttonDeletePoint";
            buttonDeletePoint.Size = new Size(71, 41);
            buttonDeletePoint.TabIndex = 3;
            buttonDeletePoint.UseVisualStyleBackColor = false;
            buttonDeletePoint.Click += buttonDeletePoint_Click;
            // 
            // ChangePoint
            // 
            ChangePoint.BackColor = Color.LightGray;
            ChangePoint.BackgroundImageLayout = ImageLayout.Center;
            ChangePoint.Cursor = Cursors.Hand;
            ChangePoint.Dock = DockStyle.Fill;
            ChangePoint.FlatAppearance.BorderSize = 0;
            ChangePoint.FlatStyle = FlatStyle.Flat;
            ChangePoint.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            ChangePoint.Location = new Point(234, 3);
            ChangePoint.Name = "ChangePoint";
            ChangePoint.Size = new Size(228, 41);
            ChangePoint.TabIndex = 2;
            ChangePoint.Text = "Редагувати";
            ChangePoint.UseVisualStyleBackColor = false;
            ChangePoint.Click += ChangePoint_Click;
            // 
            // buttonAddPoint
            // 
            buttonAddPoint.BackColor = Color.Transparent;
            buttonAddPoint.BackgroundImage = Properties.Resources.plus_icon_plus_svg_png_icon_download_1;
            buttonAddPoint.BackgroundImageLayout = ImageLayout.Center;
            buttonAddPoint.Cursor = Cursors.Hand;
            buttonAddPoint.Dock = DockStyle.Fill;
            buttonAddPoint.FlatAppearance.BorderSize = 0;
            buttonAddPoint.FlatStyle = FlatStyle.Flat;
            buttonAddPoint.Location = new Point(3, 3);
            buttonAddPoint.Name = "buttonAddPoint";
            buttonAddPoint.Size = new Size(71, 41);
            buttonAddPoint.TabIndex = 1;
            buttonAddPoint.UseVisualStyleBackColor = false;
            buttonAddPoint.Click += buttonAddPoint_Click;
            // 
            // buttonAddFavorite
            // 
            buttonAddFavorite.BackColor = Color.Transparent;
            buttonAddFavorite.BackgroundImage = Properties.Resources._2267;
            buttonAddFavorite.BackgroundImageLayout = ImageLayout.Center;
            buttonAddFavorite.Cursor = Cursors.Hand;
            buttonAddFavorite.Dock = DockStyle.Fill;
            buttonAddFavorite.FlatAppearance.BorderSize = 0;
            buttonAddFavorite.FlatStyle = FlatStyle.Flat;
            buttonAddFavorite.Location = new Point(80, 3);
            buttonAddFavorite.Name = "buttonAddFavorite";
            buttonAddFavorite.Size = new Size(71, 41);
            buttonAddFavorite.TabIndex = 0;
            buttonAddFavorite.UseVisualStyleBackColor = false;
            buttonAddFavorite.Click += buttonAddFavorite_Click;
            // 
            // TableFavoritePoints
            // 
            TableFavoritePoints.ColumnCount = 1;
            TableFavoritePoints.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            TableFavoritePoints.Controls.Add(PanelFavoritePoints, 0, 1);
            TableFavoritePoints.Controls.Add(tableFavoriteTop, 0, 0);
            TableFavoritePoints.Dock = DockStyle.Fill;
            TableFavoritePoints.Location = new Point(0, 0);
            TableFavoritePoints.Name = "TableFavoritePoints";
            TableFavoritePoints.RowCount = 2;
            TableFavoritePoints.RowStyles.Add(new RowStyle(SizeType.Percent, 7.88321161F));
            TableFavoritePoints.RowStyles.Add(new RowStyle(SizeType.Percent, 92.11679F));
            TableFavoritePoints.Size = new Size(945, 724);
            TableFavoritePoints.TabIndex = 2;
            // 
            // PanelFavoritePoints
            // 
            PanelFavoritePoints.AutoScroll = true;
            PanelFavoritePoints.Dock = DockStyle.Fill;
            PanelFavoritePoints.Location = new Point(0, 57);
            PanelFavoritePoints.Margin = new Padding(0);
            PanelFavoritePoints.Name = "PanelFavoritePoints";
            PanelFavoritePoints.Size = new Size(945, 667);
            PanelFavoritePoints.TabIndex = 1;
            // 
            // tableFavoriteTop
            // 
            tableFavoriteTop.ColumnCount = 3;
            tableFavoriteTop.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            tableFavoriteTop.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 25F));
            tableFavoriteTop.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableFavoriteTop.Controls.Add(tableLayoutPanel6, 0, 0);
            tableFavoriteTop.Controls.Add(tableFavoriteButtonTop, 2, 0);
            tableFavoriteTop.Dock = DockStyle.Fill;
            tableFavoriteTop.Location = new Point(3, 3);
            tableFavoriteTop.Name = "tableFavoriteTop";
            tableFavoriteTop.RowCount = 1;
            tableFavoriteTop.RowStyles.Add(new RowStyle(SizeType.Percent, 25F));
            tableFavoriteTop.Size = new Size(939, 51);
            tableFavoriteTop.TabIndex = 0;
            // 
            // tableLayoutPanel6
            // 
            tableLayoutPanel6.ColumnCount = 2;
            tableLayoutPanel6.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 30F));
            tableLayoutPanel6.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel6.Controls.Add(buttonFavoriteBack, 0, 0);
            tableLayoutPanel6.Controls.Add(labelFavoritePoints, 1, 0);
            tableLayoutPanel6.Dock = DockStyle.Fill;
            tableLayoutPanel6.Location = new Point(3, 3);
            tableLayoutPanel6.Name = "tableLayoutPanel6";
            tableLayoutPanel6.RowCount = 1;
            tableLayoutPanel6.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel6.Size = new Size(228, 45);
            tableLayoutPanel6.TabIndex = 1;
            // 
            // buttonFavoriteBack
            // 
            buttonFavoriteBack.BackColor = Color.Transparent;
            buttonFavoriteBack.Cursor = Cursors.Hand;
            buttonFavoriteBack.Dock = DockStyle.Fill;
            buttonFavoriteBack.FlatAppearance.BorderSize = 0;
            buttonFavoriteBack.FlatStyle = FlatStyle.Flat;
            buttonFavoriteBack.ForeColor = Color.Transparent;
            buttonFavoriteBack.Image = (Image)resources.GetObject("buttonFavoriteBack.Image");
            buttonFavoriteBack.Location = new Point(3, 3);
            buttonFavoriteBack.Name = "buttonFavoriteBack";
            buttonFavoriteBack.Size = new Size(24, 39);
            buttonFavoriteBack.TabIndex = 0;
            buttonFavoriteBack.UseVisualStyleBackColor = false;
            buttonFavoriteBack.Click += buttonFavoriteBack_Click;
            // 
            // labelFavoritePoints
            // 
            labelFavoritePoints.AutoSize = true;
            labelFavoritePoints.BackColor = Color.Transparent;
            labelFavoritePoints.Dock = DockStyle.Fill;
            labelFavoritePoints.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            labelFavoritePoints.Location = new Point(33, 0);
            labelFavoritePoints.Name = "labelFavoritePoints";
            labelFavoritePoints.Size = new Size(192, 45);
            labelFavoritePoints.TabIndex = 1;
            labelFavoritePoints.Text = "Обране";
            labelFavoritePoints.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // tableFavoriteButtonTop
            // 
            tableFavoriteButtonTop.ColumnCount = 2;
            tableFavoriteButtonTop.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableFavoriteButtonTop.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableFavoriteButtonTop.Controls.Add(SaveFavorite, 1, 0);
            tableFavoriteButtonTop.Controls.Add(DeleteFavorite, 0, 0);
            tableFavoriteButtonTop.Dock = DockStyle.Fill;
            tableFavoriteButtonTop.Location = new Point(468, 0);
            tableFavoriteButtonTop.Margin = new Padding(0);
            tableFavoriteButtonTop.Name = "tableFavoriteButtonTop";
            tableFavoriteButtonTop.RowCount = 1;
            tableFavoriteButtonTop.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableFavoriteButtonTop.Size = new Size(471, 51);
            tableFavoriteButtonTop.TabIndex = 2;
            // 
            // SaveFavorite
            // 
            SaveFavorite.BackColor = Color.LightGray;
            SaveFavorite.Cursor = Cursors.Hand;
            SaveFavorite.Dock = DockStyle.Fill;
            SaveFavorite.FlatAppearance.BorderSize = 0;
            SaveFavorite.FlatStyle = FlatStyle.Flat;
            SaveFavorite.Font = new Font("Segoe UI", 11.25F, FontStyle.Regular, GraphicsUnit.Point);
            SaveFavorite.Location = new Point(238, 3);
            SaveFavorite.Name = "SaveFavorite";
            SaveFavorite.Size = new Size(230, 45);
            SaveFavorite.TabIndex = 1;
            SaveFavorite.Text = "Зберегти";
            SaveFavorite.UseVisualStyleBackColor = false;
            SaveFavorite.Click += SaveFavorite_Click;
            // 
            // DeleteFavorite
            // 
            DeleteFavorite.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            DeleteFavorite.BackColor = Color.Transparent;
            DeleteFavorite.Cursor = Cursors.Hand;
            DeleteFavorite.FlatAppearance.BorderSize = 0;
            DeleteFavorite.FlatStyle = FlatStyle.Flat;
            DeleteFavorite.Image = Properties.Resources.image_processing20210629_14744_1rjf38b;
            DeleteFavorite.Location = new Point(159, 6);
            DeleteFavorite.Name = "DeleteFavorite";
            DeleteFavorite.Size = new Size(73, 42);
            DeleteFavorite.TabIndex = 0;
            DeleteFavorite.UseVisualStyleBackColor = false;
            DeleteFavorite.Click += DeleteFavorite_Click;
            // 
            // Shops
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(945, 724);
            Controls.Add(TaibleMainMenu);
            Controls.Add(TableFavoritePoints);
            Controls.Add(TableAllPoints);
            Name = "Shops";
            Text = "Trade Points";
            FormClosing += Shops_FormClosing;
            Load += Shops_Load;
            TaibleMainMenu.ResumeLayout(false);
            TableAllPoints.ResumeLayout(false);
            TableAllSearch.ResumeLayout(false);
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel1.PerformLayout();
            AllPointsBackAndName.ResumeLayout(false);
            AllPointsBackAndName.PerformLayout();
            SearchNameTable.ResumeLayout(false);
            SearchNameTable.PerformLayout();
            tableLayoutPanel2.ResumeLayout(false);
            tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureSearch).EndInit();
            tableForActionByPoints.ResumeLayout(false);
            TableFavoritePoints.ResumeLayout(false);
            tableFavoriteTop.ResumeLayout(false);
            tableLayoutPanel6.ResumeLayout(false);
            tableLayoutPanel6.PerformLayout();
            tableFavoriteButtonTop.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private TableLayoutPanel TaibleMainMenu;
        private Button TradePointButton;
        private Button ExitButton;
        private Button FavoriteButton;
        private TableLayoutPanel TableAllPoints;
        private TableLayoutPanel TableAllSearch;
        private TableLayoutPanel AllPointsBackAndName;
        private Button AllPointsBackButton;
        private Label AllPointsName;
        private TableLayoutPanel SearchNameTable;
        private Label labelNameSearch;
        private TableLayoutPanel tableLayoutPanel1;
        private Label labelAdressSearch;
        private TableLayoutPanel tableLayoutPanel2;
        private Label labelSpecializationSearch;
        private PictureBox pictureSearch;
        private TextBox searchByAdress;
        private TextBox searchByName;
        private TextBox searchBySpecialization;
        private TableLayoutPanel tableForActionByPoints;
        private Button buttonAddFavorite;
        private Button buttonDeletePoint;
        private Button ChangePoint;
        private Button buttonAddPoint;
        private FlowLayoutPanel PanelAllPoints;
        private TableLayoutPanel TableFavoritePoints;
        private FlowLayoutPanel PanelFavoritePoints;
        private TableLayoutPanel tableFavoriteTop;
        private TableLayoutPanel tableLayoutPanel6;
        private Button buttonFavoriteBack;
        private Label labelFavoritePoints;
        private TableLayoutPanel tableFavoriteButtonTop;
        private Button SaveFavorite;
        private Button DeleteFavorite;
        private SaveFileDialog saveFileDialog1;
    }
}