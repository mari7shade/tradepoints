﻿namespace TradePoints
{
    partial class AddToFavorite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            AddLabell = new Label();
            AddButton = new Button();
            NotAdd = new Button();
            SuspendLayout();
            // 
            // AddLabell
            // 
            AddLabell.AutoSize = true;
            AddLabell.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            AddLabell.Location = new Point(31, 100);
            AddLabell.Name = "AddLabell";
            AddLabell.Size = new Size(427, 32);
            AddLabell.TabIndex = 0;
            AddLabell.Text = "Додати торгову точку до обранного?";
            // 
            // AddButton
            // 
            AddButton.Cursor = Cursors.Hand;
            AddButton.FlatAppearance.BorderSize = 0;
            AddButton.FlatStyle = FlatStyle.Flat;
            AddButton.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            AddButton.Location = new Point(202, 213);
            AddButton.Name = "AddButton";
            AddButton.Size = new Size(101, 56);
            AddButton.TabIndex = 1;
            AddButton.Text = "Так";
            AddButton.UseVisualStyleBackColor = true;
            AddButton.Click += AddButton_Click;
            // 
            // NotAdd
            // 
            NotAdd.Cursor = Cursors.Hand;
            NotAdd.FlatAppearance.BorderSize = 0;
            NotAdd.FlatStyle = FlatStyle.Flat;
            NotAdd.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            NotAdd.Location = new Point(202, 308);
            NotAdd.Name = "NotAdd";
            NotAdd.Size = new Size(101, 56);
            NotAdd.TabIndex = 2;
            NotAdd.Text = "Ні";
            NotAdd.UseVisualStyleBackColor = true;
            NotAdd.Click += NotAdd_Click;
            // 
            // AddToFavorite
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(489, 466);
            Controls.Add(NotAdd);
            Controls.Add(AddButton);
            Controls.Add(AddLabell);
            Name = "AddToFavorite";
            Text = "AddToFavorite";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label AddLabell;
        private Button AddButton;
        private Button NotAdd;
    }
}