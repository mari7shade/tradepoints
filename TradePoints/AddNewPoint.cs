﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TradePoints.Models;

namespace TradePoints
{
    public partial class AddNewPoint : Form
    {
        public List<PointDataModel> _pointsList = new List<PointDataModel>();
        public AddNewPoint(List<PointDataModel> pointsList)
        {
            InitializeComponent();
            _pointsList = pointsList;
        }
        private void AddNewPointButton_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text.Length > 0 && textBoxAdress.Text.Length > 0 && textBoxOwnership.Text.Length > 0 && textBoxPhone.Text.Length > 0 && textBoxSpecialization.Text.Length > 0 && textBoxWorkingHours.Text.Length > 0)
            {
                PointDataModel point = new PointDataModel
                {
                    Name = textBoxName.Text,
                    Address = textBoxAdress.Text,
                    Telephone = textBoxPhone.Text,
                    Specialization = textBoxSpecialization.Text,
                    Ownership = textBoxOwnership.Text,
                    WorkingHours = textBoxWorkingHours.Text,
                    Image = textBoxImageURL.Text,
                    Favorit = false
                };
                _pointsList.Add(point);
                MessageBox.Show("Точка додана до списку!");
                this.Close();
            }
            else { MessageBox.Show("Заповніть всі поля форми"); }
        }

        private void buttonClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
