﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TradePoints.Models;

namespace TradePoints
{
    public partial class DeletePoint : Form
    {
        public List<PointDataModel> _pointsList = new List<PointDataModel>();
        List<PointAppearance> _pointsClickModel = new List<PointAppearance>();
        public DeletePoint(List<PointDataModel> pointsList, List<PointAppearance> pointsClickModel)
        {
            InitializeComponent();

            _pointsList = pointsList;
            _pointsClickModel = pointsClickModel;
        }
        void DeletePointMethod()
        {
            for (int i = (_pointsList.Count-1); i >=0 ; i--)
            {
                if (_pointsClickModel[i].PointName == _pointsList[i].Name && _pointsClickModel[i].Cheked == true)
                {
                    _pointsList.Remove(_pointsList[i]);
                }
            }
        }
        private void DeleteButton_Click(object sender, EventArgs e)
        {
            DeletePointMethod();
            MessageBox.Show("Успішно видалено!");
            this.Close();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
