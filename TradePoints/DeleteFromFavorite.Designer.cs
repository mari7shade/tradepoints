﻿namespace TradePoints
{
    partial class DeleteFromFavorite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DeleteFavoriteLabel = new Label();
            YesButton = new Button();
            NoButton = new Button();
            SuspendLayout();
            // 
            // DeleteFavoriteLabel
            // 
            DeleteFavoriteLabel.AutoSize = true;
            DeleteFavoriteLabel.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            DeleteFavoriteLabel.Location = new Point(32, 99);
            DeleteFavoriteLabel.Name = "DeleteFavoriteLabel";
            DeleteFavoriteLabel.Size = new Size(435, 32);
            DeleteFavoriteLabel.TabIndex = 1;
            DeleteFavoriteLabel.Text = "Видалити торгову точку з обранного?";
            // 
            // YesButton
            // 
            YesButton.Cursor = Cursors.Hand;
            YesButton.FlatAppearance.BorderSize = 0;
            YesButton.FlatStyle = FlatStyle.Flat;
            YesButton.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            YesButton.Location = new Point(194, 205);
            YesButton.Name = "YesButton";
            YesButton.Size = new Size(101, 56);
            YesButton.TabIndex = 2;
            YesButton.Text = "Так";
            YesButton.UseVisualStyleBackColor = true;
            YesButton.Click += YesButton_Click;
            // 
            // NoButton
            // 
            NoButton.Cursor = Cursors.Hand;
            NoButton.FlatAppearance.BorderSize = 0;
            NoButton.FlatStyle = FlatStyle.Flat;
            NoButton.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            NoButton.Location = new Point(194, 313);
            NoButton.Name = "NoButton";
            NoButton.Size = new Size(101, 56);
            NoButton.TabIndex = 3;
            NoButton.Text = "Ні";
            NoButton.UseVisualStyleBackColor = true;
            NoButton.Click += NoButton_Click;
            // 
            // DeleteFromFavorite
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(489, 466);
            Controls.Add(NoButton);
            Controls.Add(YesButton);
            Controls.Add(DeleteFavoriteLabel);
            Name = "DeleteFromFavorite";
            Text = "DeleteFromFavorite";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label DeleteFavoriteLabel;
        private Button YesButton;
        private Button NoButton;
    }
}