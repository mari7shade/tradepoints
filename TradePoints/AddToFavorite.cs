﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TradePoints.Models;

namespace TradePoints
{
    public partial class AddToFavorite : Form
    {
        public List<PointDataModel> _pointsList = new List<PointDataModel>();
        List<PointAppearance> _pointsClickModel = new List<PointAppearance>();
        public AddToFavorite(List<PointDataModel> pointsList, List<PointAppearance> pointsClickModel)
        {
            InitializeComponent();
            _pointsList = pointsList;
            _pointsClickModel = pointsClickModel;
        }
        void AddToFavoriteMethod()
        {

            for (int i = 0; i < _pointsList.Count; i++)
            {
                if (_pointsClickModel[i].PointName == _pointsList[i].Name && _pointsClickModel[i].Cheked == true)
                {
                    _pointsList[i].Favorit = true;
                }
            }
        }

        private void NotAdd_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            AddToFavoriteMethod();
            MessageBox.Show("Успішно додано до обраного!");
            this.Close();
        }
    }
}
