﻿namespace TradePoints
{
    partial class DeletePoint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DeleteLabel = new Label();
            DeleteButton = new Button();
            ExitButton = new Button();
            SuspendLayout();
            // 
            // DeleteLabel
            // 
            DeleteLabel.AutoSize = true;
            DeleteLabel.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            DeleteLabel.Location = new Point(84, 101);
            DeleteLabel.Name = "DeleteLabel";
            DeleteLabel.Size = new Size(291, 32);
            DeleteLabel.TabIndex = 1;
            DeleteLabel.Text = "Видалити торгову точку?";
            // 
            // DeleteButton
            // 
            DeleteButton.Cursor = Cursors.Hand;
            DeleteButton.FlatAppearance.BorderSize = 0;
            DeleteButton.FlatStyle = FlatStyle.Flat;
            DeleteButton.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            DeleteButton.Location = new Point(179, 200);
            DeleteButton.Name = "DeleteButton";
            DeleteButton.Size = new Size(101, 56);
            DeleteButton.TabIndex = 2;
            DeleteButton.Text = "Так";
            DeleteButton.UseVisualStyleBackColor = true;
            DeleteButton.Click += DeleteButton_Click;
            // 
            // ExitButton
            // 
            ExitButton.Cursor = Cursors.Hand;
            ExitButton.FlatAppearance.BorderSize = 0;
            ExitButton.FlatStyle = FlatStyle.Flat;
            ExitButton.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            ExitButton.Location = new Point(179, 294);
            ExitButton.Name = "ExitButton";
            ExitButton.Size = new Size(101, 56);
            ExitButton.TabIndex = 3;
            ExitButton.Text = "Ні";
            ExitButton.UseVisualStyleBackColor = true;
            ExitButton.Click += ExitButton_Click;
            // 
            // DeletePoint
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(489, 466);
            Controls.Add(ExitButton);
            Controls.Add(DeleteButton);
            Controls.Add(DeleteLabel);
            Name = "DeletePoint";
            Text = "DeletePoint";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label DeleteLabel;
        private Button DeleteButton;
        private Button ExitButton;
    }
}