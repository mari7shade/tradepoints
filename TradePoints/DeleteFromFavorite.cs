﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TradePoints.Models;

namespace TradePoints
{
    public partial class DeleteFromFavorite : Form
    {
        public List<PointDataModel> _pointsList = new List<PointDataModel>();
        List<PointAppearance> _pointsClickModel = new List<PointAppearance>();
        public DeleteFromFavorite(List<PointDataModel> pointsList, List<PointAppearance> pointsClickModel)
        {
            InitializeComponent();
            _pointsList = pointsList;
            _pointsClickModel = pointsClickModel;
        }
        void DeleteFromFavoriteMethod()
        {

            for (int i = 0; i < _pointsList.Count; i++)
            {
                if (_pointsClickModel[i].PointName == _pointsList[i].Name && _pointsClickModel[i].Cheked == true)
                {
                    _pointsList[i].Favorit = false;
                }
            }
        }
        private void NoButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void YesButton_Click(object sender, EventArgs e)
        {
            DeleteFromFavoriteMethod();
            MessageBox.Show("Успішно видалено з обраного!");
            this.Close();
        }
    }
}
