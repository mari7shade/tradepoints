﻿namespace TradePoints
{
    public partial class InfoPoint : Form
    {
        public InfoPoint(string name, string specialization, string adress, string ownership, string workingHours, string phone, Image image)
        {
            InitializeComponent();



            NamePointLabel.Text = name;
            PointSpecializationLabel.Text = specialization;
            PointAddressLabel.Text = adress;
            PointOwnershipLabel.Text = ownership;
            PointWorkingHoursLabel.Text = workingHours;
            PointTelephoneLabel.Text = phone;
            PointInfoImage.Image = image;
        }



        private async void InfoPoint_Load(object sender, EventArgs e)
        {
            string originalString = ("https://www.google.com/maps/place/" + PointAddressLabel.Text);
            string replacedString = originalString.Replace(" ", "+");

            //MessageBox.Show(replacedString);
            try
            {
                if (MapPoint != null)
                {
                    await MapPoint.EnsureCoreWebView2Async(null);
                    MapPoint.CoreWebView2.Navigate(replacedString);
                }
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message);
                /*string a = "https://nipunadilhara.medium.com/adding-google-maps-into-windows-form-application-in-c-46be1aec2981";
                if (MapPoint != null)
                    MapPoint.CoreWebView2.Navigate(a);*/
            }

        }
    }
}
