﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TradePoints.Models;

namespace TradePoints
{
    public partial class ChangePoint : Form
    {

        public List<PointDataModel> _pointsList = new List<PointDataModel>();
        List<PointAppearance> _pointsClickModel = new List<PointAppearance>();
        public ChangePoint(List<PointDataModel> pointsList, List<PointAppearance> pointsClickModel)
        {
            InitializeComponent();
            _pointsClickModel = pointsClickModel;
            _pointsList = pointsList;
        }
        void ChangePointMethod()
        {
            for (int i = 0; i < _pointsList.Count; i++)
            {
                if (_pointsClickModel[i].PointName == _pointsList[i].Name && _pointsClickModel[i].Cheked == true)
                {
                    _pointsList[i].Name = textBoxName.Text;
                    _pointsList[i].Address = textBoxAdress.Text;
                    _pointsList[i].Telephone = textBoxPhone.Text;
                    _pointsList[i].Ownership = textBoxOwnership.Text;
                    _pointsList[i].Specialization = textBoxSpecialization.Text;
                    _pointsList[i].WorkingHours = textBoxWorkingHours.Text;
                    _pointsList[i].Image = textBoxImageURL.Text;
                }
            }
        }
        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChangePoint_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < _pointsList.Count; i++)
            {
                if (_pointsClickModel[i].PointName == _pointsList[i].Name && _pointsClickModel[i].Cheked == true)
                {
                    textBoxName.Text = _pointsList[i].Name;
                    textBoxAdress.Text = _pointsList[i].Address;
                    textBoxOwnership.Text = _pointsList[i].Ownership;
                    textBoxPhone.Text = _pointsList[i].Telephone;
                    textBoxSpecialization.Text = _pointsList[i].Specialization;
                    textBoxWorkingHours.Text = _pointsList[i].WorkingHours;
                    textBoxImageURL.Text = _pointsList[i].Image;
                }
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            ChangePointMethod();
            MessageBox.Show("Дані успішно змінено!");
            this.Close();
        }
    }
}
