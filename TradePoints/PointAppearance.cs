﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TradePoints
{
    public partial class PointAppearance : UserControl
    {

        public PointAppearance()
        {
            InitializeComponent();
            RecursiveEventHower(tableBeckground);
            RecursiveEventClick(tableBeckground);
        }


        #region properties
        bool pointFavorite;
        public bool PointFavorite
        {
            get { return pointFavorite; }
            set
            {
                pointFavorite = value;
                if (pointFavorite == true)
                {
                    pictureFavoritePoint.Visible = true;
                }
                else pictureFavoritePoint.Visible = false;
            }
        }
        string? pointName;
        [Category("Point Name")]
        public string? PointName
        {
            get { return pointName; }
            set
            {
                pointName = value;
                NamePointLabel.Text = value;
            }
        }
        string? pointImage;
        [Category("Point Image")]
        public Image? PointImage
        {
            get { return pictureBox1.Image; }
            set
            {
                pictureBox1.Image = value;
            }
        }

        string? pointSpecialization;
        [Category("Point Specialization")]
        public string? PointSpecialization
        {
            get { return pointSpecialization; }
            set
            {
                pointSpecialization = value;
                PointSpecializationLabel.Text = value;
            }
        }

        string? pointAddress;
        [Category("Point Address")]
        public string? PointAddress
        {
            get { return pointAddress; }
            set
            {
                pointAddress = value;
                PointAddressLabel.Text = value;
            }
        }

        string? pointOwnership;
        [Category("Point Ownership")]
        public string? PointOwnership
        {
            get { return pointOwnership; }
            set
            {
                pointOwnership = value;
                PointOwnershipLabel.Text = "Форма власності: " + value;
            }
        }

        string? pointWorkingHours;
        [Category("Point Working Hours")]
        public string? PointWorkingHours
        {
            get { return pointWorkingHours; }
            set
            {
                pointWorkingHours = value;
                PointWorkingHoursLabel.Text = "Час роботи: " + value;
            }
        }

        string? pointTelephone;


        [Category("Point Telephone")]
        public string? PointTelephone
        {
            get { return pointTelephone; }
            set
            {
                pointTelephone = value;
                PointTelephoneLabel.Text = "Телефон: " + value;
            }
        }
        #endregion

        public bool Cheked = false;
        void RecursiveEventClick(Control control)
        {
            control.Click += Control_Click;
            foreach (Control child in control.Controls)
            {
                RecursiveEventClick(child);
            }
        }

        private void Control_Click(object? sender, EventArgs e)
        {
            if (Cheked == false)
            {
                tableBeckground.BackColor = Color.Silver;
                Cheked = true;
            }
            else
            {
                tableBeckground.BackColor = SystemColors.Control;
                Cheked = false;
            }
        }

        void RecursiveEventHower(Control control)
        {
            control.MouseEnter += Control_MouseEnter;
            control.MouseLeave += Control_MouseLeave;


            foreach (Control child in control.Controls)
            {
                RecursiveEventHower(child);
            }
        }

        private void Control_MouseLeave(object? sender, EventArgs e)
        {
            if (Cheked == false)
            {
                tableBeckground.BackColor = SystemColors.Control;
            }
            else
            {
                tableBeckground.BackColor = Color.Silver;
            }
        }

        private void Control_MouseEnter(object? sender, EventArgs e)
        {
            tableBeckground.BackColor = Color.LightGray;
        }

        private void pictureFavoritePoint_Click(object sender, EventArgs e)
        {

        }

        private void pictureInfo_Click(object sender, EventArgs e)
        {
            InfoPoint infoPoint = new InfoPoint(NamePointLabel.Text, PointSpecializationLabel.Text, PointAddressLabel.Text, PointOwnershipLabel.Text, PointWorkingHoursLabel.Text, PointTelephoneLabel.Text, pictureBox1.Image);
            infoPoint.Show();
            if (Cheked == false)
            {
                tableBeckground.BackColor = Color.Silver;
                Cheked = true;
            }
            else
            {
                tableBeckground.BackColor = SystemColors.Control;
                Cheked = false;
            }
        }
    }

}
