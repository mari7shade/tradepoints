using Newtonsoft.Json;
using TradePoints.Models;

namespace TradePoints
{
    public partial class Shops : Form
    {
        Config? configuration = null;
        List<PointDataModel> pointsList = new List<PointDataModel>();

        List<PointAppearance> pointsClickModel = new List<PointAppearance>();

        string config = "config.json";
        public Shops()
        {
            InitializeComponent();
            saveFileDialog1.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";


        }

        #region main menu
        private void Shops_Load(object sender, EventArgs e)
        {
            if (File.Exists(config))
            {
                using (StreamReader reader = new StreamReader(config))
                {
                    string text = reader.ReadToEnd();

                    configuration = JsonConvert.DeserializeObject<Config>(text);
                }
            }

            if (configuration != null)
            {
                if (File.Exists(configuration.PathData))
                {
                    using (StreamReader reader = new StreamReader(configuration.PathData))
                    {
                        string text = reader.ReadToEnd();

                        var result = JsonConvert.DeserializeObject<TraidingDataModel>(text);

                        if (result != null)
                        {
                            pointsList = result.TradingPoints;

                        }
                    }
                }
                else
                {
                    MessageBox.Show("���� ����� �����");
                    Application.Exit();
                }
            }
        }
        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void TradePointButton_Click(object sender, EventArgs e)
        {
            TaibleMainMenu.Visible = false;
            TableAllPoints.Visible = true;
            TableFavoritePoints.Visible = false;

            ShowSearchPoint(pointsList);

        }
        private void FavoriteButton_Click(object sender, EventArgs e)
        {
            TaibleMainMenu.Visible = false;
            TableAllPoints.Visible = false;
            TableFavoritePoints.Visible = true;

            ShowFavoritePoint(pointsList);
        }
        #endregion

        #region all points
        private void AllPointsBackButton_Click(object sender, EventArgs e)
        {
            TaibleMainMenu.Visible = true;
            TableAllPoints.Visible = false;
            TableFavoritePoints.Visible = false;
        }
        async Task<Image> ImagePoints(string url)
        {
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    byte[] imageData = await client.GetByteArrayAsync(url);

                    using (MemoryStream memoryStream = new MemoryStream(imageData))
                    {
                        return Image.FromStream(memoryStream);
                    }
                }
                catch (Exception ex)
                {
                    //MessageBox.Show($"�� ���� ��������� �� ���������� {url}");
                    return Properties.Resources.DefaultImage;
                }
            }
        }
        async void ShowSearchPoint(List<PointDataModel> pointsList)
        {
            PanelAllPoints.Controls.Clear();
            pointsClickModel.Clear();

            for (int i = 0; i < pointsList.Count; i++)
            {
                PointAppearance point = new PointAppearance()
                {
                    PointName = pointsList[i].Name,
                    PointSpecialization = pointsList[i].Specialization,
                    PointOwnership = pointsList[i].Ownership,
                    PointAddress = pointsList[i].Address,
                    PointTelephone = pointsList[i].Telephone,
                    PointWorkingHours = pointsList[i].WorkingHours,
                    PointFavorite = pointsList[i].Favorit,
                };
                if (pointsList[i].Image != null)
                {
                    point.PointImage = await ImagePoints(pointsList[i].Image);
                }
                pointsClickModel.Add(point);
                PanelAllPoints.Controls.Add(point);
            }

        }
        #region search points
        private void searchByName_TextChanged(object sender, EventArgs e)
        {
            Control element = (Control)sender;

            if (element.Text.Length == 0 || element.Text.Length >= 2)
            {
                var result = pointsList.Where(p => p.Name.ToLower().Contains(element.Text.ToLower())).ToList();
                ShowSearchPoint(result);
            }
        }

        private void searchBySpecialization_TextChanged(object sender, EventArgs e)
        {
            Control element = (Control)sender;
            if (element.Text.Length == 0 || element.Text.Length >= 2)
            {
                var result = pointsList.Where(p => p.Specialization.ToLower().Contains(element.Text.ToLower())).ToList();
                ShowSearchPoint(result);
            }
        }

        private void searchByAdress_TextChanged(object sender, EventArgs e)
        {
            Control element = (Control)sender;
            if (element.Text.Length == 0 || element.Text.Length >= 2)
            {
                var result = pointsList.Where(p => p.Address.ToLower().Contains(element.Text.ToLower())).ToList();
                ShowSearchPoint(result);
            }

        }
        #endregion

        bool ZeroPointCheck()
        {
            int point = 0;
            for (int i = 0; i < pointsClickModel.Count; i++)
            {
                if (pointsClickModel[i].Cheked == true) { point++; }
            }
            if (point <= 0)
            {
                MessageBox.Show("�� ������ ������� ��������.");
                return false;
            }
            return true;
        }
        private void buttonAddFavorite_Click(object sender, EventArgs e)
        {
            if (ZeroPointCheck() == true)
            {
                AddToFavorite add = new AddToFavorite(pointsList, pointsClickModel);
                add.ShowDialog();
                pointsList = add._pointsList;
                ShowSearchPoint(pointsList);
                add.Dispose();
            }
        }
        private void buttonAddPoint_Click(object sender, EventArgs e)
        {
            AddNewPoint addNewPoint = new AddNewPoint(pointsList);
            addNewPoint.ShowDialog();
            pointsList = addNewPoint._pointsList;
            ShowSearchPoint(pointsList);
        }

        private void buttonDeletePoint_Click(object sender, EventArgs e)
        {
            if (ZeroPointCheck() == true)
            {
                DeletePoint deletePoint = new DeletePoint(pointsList, pointsClickModel);
                deletePoint.ShowDialog();
                pointsList = deletePoint._pointsList;
                ShowSearchPoint(pointsList);
            }
        }


        bool MultiPointCheck()
        {
            int point = 0;
            for (int i = 0; i < pointsClickModel.Count; i++)
            {
                if (pointsClickModel[i].Cheked == true) { point++; }
            }
            if (point > 1)
            {
                MessageBox.Show("������ ����� 1 ������� ��� �����������");
                return false;
            }
            return true;
        }
        private void ChangePoint_Click(object sender, EventArgs e)
        {
            if (MultiPointCheck() == true && ZeroPointCheck() == true)
            {
                ChangePoint change = new ChangePoint(pointsList, pointsClickModel);
                change.ShowDialog();
                pointsList = change._pointsList;
                ShowSearchPoint(pointsList);
            }
        }


        #endregion

        #region favorite
        private void buttonFavoriteBack_Click(object sender, EventArgs e)
        {
            TaibleMainMenu.Visible = true;
            TableAllPoints.Visible = false;
            TableFavoritePoints.Visible = false;
        }
        List<PointAppearance> pointsFavoriteClickModel = new List<PointAppearance>();
        async void ShowFavoritePoint(List<PointDataModel> pointsList)
        {
            pointsFavoriteClickModel.Clear();
            PanelFavoritePoints.Controls.Clear();

            for (int i = 0; i < pointsList.Count; i++)
            {
                PointAppearance point = new PointAppearance()
                {
                    PointName = pointsList[i].Name,
                    PointSpecialization = pointsList[i].Specialization,
                    PointOwnership = pointsList[i].Ownership,
                    PointAddress = pointsList[i].Address,
                    PointTelephone = pointsList[i].Telephone,
                    PointWorkingHours = pointsList[i].WorkingHours,
                    PointFavorite = pointsList[i].Favorit,
                };
                if (pointsList[i].Image != null)
                {
                    point.PointImage = await ImagePoints(pointsList[i].Image);
                }
                pointsFavoriteClickModel.Add(point);
                if (pointsList[i].Favorit == true)
                {
                    PanelFavoritePoints.Controls.Add(point);
                }
            }
        }
        bool ZeroPointFavoriteCheck()
        {
            int point = 0;
            for (int i = 0; i < pointsFavoriteClickModel.Count; i++)
            {
                if (pointsFavoriteClickModel[i].Cheked == true) { point++; }
            }
            if (point <= 0)
            {
                MessageBox.Show("�� ������ ������� ��������.");
                return false;
            }
            return true;
        }
        private void DeleteFavorite_Click(object sender, EventArgs e)
        {
            if (ZeroPointFavoriteCheck() == true)
            {
                DeleteFromFavorite deleteFromFavorite = new DeleteFromFavorite(pointsList, pointsFavoriteClickModel);
                deleteFromFavorite.ShowDialog();
                pointsList = deleteFromFavorite._pointsList;
                ShowFavoritePoint(pointsList);

            }
        }

        private void SaveFavorite_Click(object sender, EventArgs e)
        {
            List<PointDataModel> favorite = pointsList.Where(p => p.Favorit == true).ToList();
            string json = JsonConvert.SerializeObject(favorite);

            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            string filename = saveFileDialog1.FileName;
            File.WriteAllText(filename, json);
            MessageBox.Show("���������");
        }
        #endregion

        private void Shops_FormClosing(object sender, FormClosingEventArgs e)
        {

            using (StreamWriter writer = new StreamWriter(configuration.PathData))
            {
                var result = JsonConvert.SerializeObject(new TraidingDataModel { TradingPoints = pointsList });
                writer.Write(result);
            }
        }
    }
}