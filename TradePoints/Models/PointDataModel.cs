﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradePoints.Models
{
    public class PointDataModel
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
        public string Specialization { get; set; }
        public string Ownership { get; set; }
        [JsonProperty("“WorkingHours”")]
        public string WorkingHours { get; set; }
        public string Image { get; set; }
        public bool Favorit { get; set; }
    }
}
