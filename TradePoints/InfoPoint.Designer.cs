﻿namespace TradePoints
{
    partial class InfoPoint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            MainLabel = new TableLayoutPanel();
            tableLayoutInfo = new TableLayoutPanel();
            PointInfoImage = new PictureBox();
            tableLayoutInfoPoint = new TableLayoutPanel();
            PointSpecializationLabel = new Label();
            PointAddressLabel = new Label();
            PointOwnershipLabel = new Label();
            PointWorkingHoursLabel = new Label();
            PointTelephoneLabel = new Label();
            NamePointLabel = new Label();
            MapPoint = new Microsoft.Web.WebView2.WinForms.WebView2();
            MainLabel.SuspendLayout();
            tableLayoutInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)PointInfoImage).BeginInit();
            tableLayoutInfoPoint.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)MapPoint).BeginInit();
            SuspendLayout();
            // 
            // MainLabel
            // 
            MainLabel.ColumnCount = 1;
            MainLabel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            MainLabel.Controls.Add(tableLayoutInfo, 0, 1);
            MainLabel.Controls.Add(NamePointLabel, 0, 0);
            MainLabel.Controls.Add(MapPoint, 0, 2);
            MainLabel.Dock = DockStyle.Fill;
            MainLabel.Location = new Point(0, 0);
            MainLabel.Name = "MainLabel";
            MainLabel.RowCount = 3;
            MainLabel.RowStyles.Add(new RowStyle(SizeType.Percent, 7.94094229F));
            MainLabel.RowStyles.Add(new RowStyle(SizeType.Percent, 40.7035179F));
            MainLabel.RowStyles.Add(new RowStyle(SizeType.Percent, 51.4237862F));
            MainLabel.Size = new Size(827, 597);
            MainLabel.TabIndex = 0;
            // 
            // tableLayoutInfo
            // 
            tableLayoutInfo.ColumnCount = 2;
            tableLayoutInfo.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 30F));
            tableLayoutInfo.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 70F));
            tableLayoutInfo.Controls.Add(PointInfoImage, 0, 0);
            tableLayoutInfo.Controls.Add(tableLayoutInfoPoint, 1, 0);
            tableLayoutInfo.Dock = DockStyle.Fill;
            tableLayoutInfo.Location = new Point(3, 50);
            tableLayoutInfo.Name = "tableLayoutInfo";
            tableLayoutInfo.RowCount = 1;
            tableLayoutInfo.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutInfo.Size = new Size(821, 236);
            tableLayoutInfo.TabIndex = 0;
            // 
            // PointInfoImage
            // 
            PointInfoImage.Dock = DockStyle.Fill;
            PointInfoImage.Location = new Point(3, 3);
            PointInfoImage.Name = "PointInfoImage";
            PointInfoImage.Size = new Size(240, 230);
            PointInfoImage.SizeMode = PictureBoxSizeMode.Zoom;
            PointInfoImage.TabIndex = 0;
            PointInfoImage.TabStop = false;
            // 
            // tableLayoutInfoPoint
            // 
            tableLayoutInfoPoint.ColumnCount = 1;
            tableLayoutInfoPoint.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutInfoPoint.Controls.Add(PointSpecializationLabel, 0, 0);
            tableLayoutInfoPoint.Controls.Add(PointAddressLabel, 0, 1);
            tableLayoutInfoPoint.Controls.Add(PointOwnershipLabel, 0, 2);
            tableLayoutInfoPoint.Controls.Add(PointWorkingHoursLabel, 0, 3);
            tableLayoutInfoPoint.Controls.Add(PointTelephoneLabel, 0, 4);
            tableLayoutInfoPoint.Dock = DockStyle.Fill;
            tableLayoutInfoPoint.Location = new Point(249, 3);
            tableLayoutInfoPoint.Name = "tableLayoutInfoPoint";
            tableLayoutInfoPoint.RowCount = 5;
            tableLayoutInfoPoint.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutInfoPoint.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutInfoPoint.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutInfoPoint.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutInfoPoint.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutInfoPoint.Size = new Size(569, 230);
            tableLayoutInfoPoint.TabIndex = 1;
            // 
            // PointSpecializationLabel
            // 
            PointSpecializationLabel.AutoSize = true;
            PointSpecializationLabel.Dock = DockStyle.Fill;
            PointSpecializationLabel.Font = new Font("Segoe UI", 9.75F, FontStyle.Regular, GraphicsUnit.Point);
            PointSpecializationLabel.Location = new Point(15, 0);
            PointSpecializationLabel.Margin = new Padding(15, 0, 3, 0);
            PointSpecializationLabel.Name = "PointSpecializationLabel";
            PointSpecializationLabel.Size = new Size(551, 46);
            PointSpecializationLabel.TabIndex = 0;
            PointSpecializationLabel.Text = "label1";
            PointSpecializationLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // PointAddressLabel
            // 
            PointAddressLabel.AutoSize = true;
            PointAddressLabel.Dock = DockStyle.Fill;
            PointAddressLabel.Font = new Font("Segoe UI", 9.75F, FontStyle.Regular, GraphicsUnit.Point);
            PointAddressLabel.Location = new Point(15, 46);
            PointAddressLabel.Margin = new Padding(15, 0, 3, 0);
            PointAddressLabel.Name = "PointAddressLabel";
            PointAddressLabel.Size = new Size(551, 46);
            PointAddressLabel.TabIndex = 1;
            PointAddressLabel.Text = "label2";
            PointAddressLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // PointOwnershipLabel
            // 
            PointOwnershipLabel.AutoSize = true;
            PointOwnershipLabel.Dock = DockStyle.Fill;
            PointOwnershipLabel.Font = new Font("Segoe UI", 9.75F, FontStyle.Regular, GraphicsUnit.Point);
            PointOwnershipLabel.Location = new Point(15, 92);
            PointOwnershipLabel.Margin = new Padding(15, 0, 3, 0);
            PointOwnershipLabel.Name = "PointOwnershipLabel";
            PointOwnershipLabel.Size = new Size(551, 46);
            PointOwnershipLabel.TabIndex = 2;
            PointOwnershipLabel.Text = "label3";
            PointOwnershipLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // PointWorkingHoursLabel
            // 
            PointWorkingHoursLabel.AutoSize = true;
            PointWorkingHoursLabel.Dock = DockStyle.Fill;
            PointWorkingHoursLabel.Font = new Font("Segoe UI", 9.75F, FontStyle.Regular, GraphicsUnit.Point);
            PointWorkingHoursLabel.Location = new Point(15, 138);
            PointWorkingHoursLabel.Margin = new Padding(15, 0, 3, 0);
            PointWorkingHoursLabel.Name = "PointWorkingHoursLabel";
            PointWorkingHoursLabel.Size = new Size(551, 46);
            PointWorkingHoursLabel.TabIndex = 3;
            PointWorkingHoursLabel.Text = "label4";
            PointWorkingHoursLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // PointTelephoneLabel
            // 
            PointTelephoneLabel.AutoSize = true;
            PointTelephoneLabel.Dock = DockStyle.Fill;
            PointTelephoneLabel.Font = new Font("Segoe UI", 9.75F, FontStyle.Regular, GraphicsUnit.Point);
            PointTelephoneLabel.Location = new Point(15, 184);
            PointTelephoneLabel.Margin = new Padding(15, 0, 3, 0);
            PointTelephoneLabel.Name = "PointTelephoneLabel";
            PointTelephoneLabel.Size = new Size(551, 46);
            PointTelephoneLabel.TabIndex = 4;
            PointTelephoneLabel.Text = "label5";
            PointTelephoneLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // NamePointLabel
            // 
            NamePointLabel.AutoSize = true;
            NamePointLabel.Dock = DockStyle.Fill;
            NamePointLabel.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point);
            NamePointLabel.Location = new Point(3, 0);
            NamePointLabel.Name = "NamePointLabel";
            NamePointLabel.Size = new Size(821, 47);
            NamePointLabel.TabIndex = 1;
            NamePointLabel.Text = "label6";
            NamePointLabel.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // MapPoint
            // 
            MapPoint.AllowExternalDrop = true;
            MapPoint.CreationProperties = null;
            MapPoint.DefaultBackgroundColor = Color.White;
            MapPoint.Dock = DockStyle.Fill;
            MapPoint.Location = new Point(3, 292);
            MapPoint.Name = "MapPoint";
            MapPoint.Size = new Size(821, 302);
            MapPoint.TabIndex = 2;
            MapPoint.ZoomFactor = 1D;
            // 
            // InfoPoint
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(827, 597);
            Controls.Add(MainLabel);
            Name = "InfoPoint";
            Text = "InfoPoint";
            Load += InfoPoint_Load;
            MainLabel.ResumeLayout(false);
            MainLabel.PerformLayout();
            tableLayoutInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)PointInfoImage).EndInit();
            tableLayoutInfoPoint.ResumeLayout(false);
            tableLayoutInfoPoint.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)MapPoint).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private TableLayoutPanel MainLabel;
        private TableLayoutPanel tableLayoutInfo;
        private PictureBox PointInfoImage;
        private TableLayoutPanel tableLayoutInfoPoint;
        private Label PointSpecializationLabel;
        private Label PointAddressLabel;
        private Label PointOwnershipLabel;
        private Label PointWorkingHoursLabel;
        private Label PointTelephoneLabel;
        private Label NamePointLabel;
        private Microsoft.Web.WebView2.WinForms.WebView2 MapPoint;
    }
}